using TestProject.Common.DTO.ExecutionResult;
using TestProject.Common.Enums;
using System;
using System.Reflection;
using TestProject.SDK;

namespace TestProject.Generated.Tests.ActiveTime
{
	/// <summary>
	/// This class was automatically generated by TestProject
	/// Provides an example how to run coded TestProject tests
	/// </summary>
	public class ActivetimeRunner
	{
		public static string DevToken = "qn0kC3Ce3bugGOycd7LAjpmtaQEb4U5FpkDF6OvO1DI";
		public static AutomatedBrowserType Browser = AutomatedBrowserType.Chrome;
		public static StepExecutionResult RunCP001RegistrarUsuario()
		{
			using (var runner = new RunnerBuilder(DevToken).AsWeb(Browser).Build())
				return runner.Run(new CP001RegistrarUsuario(), true);
		}

		public static void Main(string[] args)
		{
			try
			{
				Assembly.LoadFrom("Addons/Web-Extensions.proxy.dll");
				RunCP001RegistrarUsuario();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex);
			}
		}
	}
}