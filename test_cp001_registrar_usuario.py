from addons.web_extensions import WebExtensions
from selenium.webdriver.common.by import By
from src.testproject.classes import DriverStepSettings, StepSettings
from src.testproject.decorator import report_assertion_errors
from src.testproject.enums import SleepTimingType
from src.testproject.sdk.drivers import webdriver
import pytest


"""
This pytest test was automatically generated by TestProject
    Project: ActiveTime
    Package: TestProject.Generated.Tests.ActiveTime
    Test: CP001 - Registrar usuario
    Generated by: Paula Garcia (mpgarcia91@ucatolica.edu.co)
    Generated on 10/10/2021, 00:17:50
"""


@pytest.fixture()
def driver():
    driver = webdriver.Chrome(token="qn0kC3Ce3bugGOycd7LAjpmtaQEb4U5FpkDF6OvO1DI",
                              project_name="ActiveTime",
                              job_name="CP001 - Registrar usuario")
    step_settings = StepSettings(timeout=15000,
                                 sleep_time=500,
                                 sleep_timing_type=SleepTimingType.Before)
    with DriverStepSettings(driver, step_settings):
        yield driver
    driver.quit()


@report_assertion_errors
def test_main(driver):
    """Prueba automática - caso exitoso."""
    # Test Parameters
    # Auto generated application URL parameter
    ApplicationURL = "http://localhost/xampp/ActiveTime/index.html"
    genero = ""
    name = ""
    email = ""
    email1 = ""
    contra = ""
    password = ""
    cel = ""
    id = ""
    datebirth = ""
    tip_id = ""

    # 1. Navigate to '{ApplicationURL}'
    # Navigates the specified URL (Auto-generated)
    driver.get(f'{ApplicationURL}')

    # 2. Click 'Sign up'
    sign_up = driver.find_element(By.CSS_SELECTOR,
                                  "#button-Register")
    sign_up.click()

    # 3. Click 'genero'
    genero = driver.find_element(By.CSS_SELECTOR,
                                 "#genero")
    genero.click()

    # 4. Select all '{genero}' options in 'genero'
    genero = (By.CSS_SELECTOR, "#genero")
    driver.addons().execute(
        WebExtensions.selectoptionbyvalue(
            value=f'{genero}'), *genero)

    # 5. Click 'genero'
    genero = driver.find_element(By.CSS_SELECTOR,
                                 "#genero")
    genero.click()

    # 6. Click 'Next'
    next = driver.find_element(By.CSS_SELECTOR,
                               "#next-genero")
    next.click()

    # 7. Click 'name'
    name = driver.find_element(By.CSS_SELECTOR,
                               "#userName")
    name.click()

    # 8. Type '{name}' in 'name'
    name = driver.find_element(By.CSS_SELECTOR,
                               "#userName")
    name.send_keys(f'{name}')

    # 9. Click 'Next1'
    next1 = driver.find_element(By.CSS_SELECTOR,
                                "#next-usuario")
    next1.click()

    # 10. Click 'email'
    email = driver.find_element(By.CSS_SELECTOR,
                                "#email")
    email.click()

    # 11. Type '{email}' in 'email'
    email = driver.find_element(By.CSS_SELECTOR,
                                "#email")
    email.send_keys(f'{email}')

    # 12. Click 'Email1'
    email1 = driver.find_element(By.CSS_SELECTOR,
                                 "#emailConfirmation")
    email1.click()

    # 13. Type '{email1}' in 'Email1'
    email1 = driver.find_element(By.CSS_SELECTOR,
                                 "#emailConfirmation")
    email1.send_keys(f'{email1}')

    # 14. Click 'Next2'
    next2 = driver.find_element(By.CSS_SELECTOR,
                                "#next-email")
    next2.click()

    # 15. Click 'contra'
    contra = driver.find_element(By.CSS_SELECTOR,
                                 "#passwordRegister")
    contra.click()

    # 16. Type '{contra}' in 'contra'
    contra = driver.find_element(By.CSS_SELECTOR,
                                 "#passwordRegister")
    contra.send_keys(f'{contra}')

    # 17. Click 'password'
    password = driver.find_element(By.CSS_SELECTOR,
                                   "#passwordConfirmation")
    password.click()

    # 18. Type '{password}' in 'password'
    password = driver.find_element(By.CSS_SELECTOR,
                                   "#passwordConfirmation")
    password.send_keys(f'{password}')

    # 19. Click 'Next3'
    next3 = driver.find_element(By.CSS_SELECTOR,
                                "#next-password")
    next3.click()

    # 20. Click 'cel'
    cel = driver.find_element(By.CSS_SELECTOR,
                              "#numTelefono")
    cel.click()

    # 21. Type '{cel}' in 'cel'
    cel = driver.find_element(By.CSS_SELECTOR,
                              "#numTelefono")
    cel.send_keys(f'{cel}')

    # 22. Click 'Next4'
    next4 = driver.find_element(By.CSS_SELECTOR,
                                "#next-Telefono")
    next4.click()

    # 23. Click 'tip_id'
    tip_id = driver.find_element(By.CSS_SELECTOR,
                                 "#identificacion")
    tip_id.click()

    # 24. Select all '{tip_id}' options in 'tip_id'
    tip_id = (By.CSS_SELECTOR, "#identificacion")
    driver.addons().execute(
        WebExtensions.selectoptionbyvalue(
            value=f'{tip_id}'), *tip_id)

    # 25. Click 'tip_id'
    tip_id = driver.find_element(By.CSS_SELECTOR,
                                 "#identificacion")
    tip_id.click()

    # 26. Click 'id'
    id = driver.find_element(By.CSS_SELECTOR,
                             "#numIdentificacion")
    id.click()

    # 27. Type '{id}' in 'id'
    id = driver.find_element(By.CSS_SELECTOR,
                             "#numIdentificacion")
    id.send_keys(f'{id}')

    # 28. Click 'Next5'
    next5 = driver.find_element(By.CSS_SELECTOR,
                                "#next-Identificacion")
    next5.click()

    # 29. Click 'datebirth'
    datebirth = driver.find_element(By.CSS_SELECTOR,
                                    "#fechaDeNacimiento")
    datebirth.click()

    # 30. Type '{datebirth}' in 'datebirth'
    datebirth = driver.find_element(By.CSS_SELECTOR,
                                    "#fechaDeNacimiento")
    datebirth.send_keys(f'{datebirth}')

    # 31. Click 'Finish Registration'
    finish_registration = driver.find_element(By.CSS_SELECTOR,
                                              "#next-fechaDeNacimiento")
    finish_registration.click()

    # 32. Click 'Home'
    home = driver.find_element(By.XPATH,
                               "//button[. = 'Home']")
    home.click()
